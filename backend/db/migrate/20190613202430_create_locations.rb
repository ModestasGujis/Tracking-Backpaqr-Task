class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.float :latitude
      t.float :longitude
      t.string :city
      t.string :country
      t.datetime :time
      t.integer :guide_id

      t.timestamps
    end
  end
end
