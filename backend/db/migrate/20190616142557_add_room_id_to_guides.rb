class AddRoomIdToGuides < ActiveRecord::Migration[5.2]
  def change
    add_column :guides, :room_id, :integer
  end
end
