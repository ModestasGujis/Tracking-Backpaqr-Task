class AddCurrLongitudeToGuides < ActiveRecord::Migration[5.2]
  def change
    add_column :guides, :curr_longitude, :float
  end
end
