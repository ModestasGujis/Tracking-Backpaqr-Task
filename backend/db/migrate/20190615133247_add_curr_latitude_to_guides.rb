class AddCurrLatitudeToGuides < ActiveRecord::Migration[5.2]
  def change
    add_column :guides, :curr_latitude, :float
  end
end
