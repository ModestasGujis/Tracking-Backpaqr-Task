class AddActiveToGuides < ActiveRecord::Migration[5.2]
  def change
    add_column :guides, :active, :boolean
  end
end
