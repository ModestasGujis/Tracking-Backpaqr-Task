# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

guides = Guide.create([
	{ name: "Petras Jonauskis", city: "Kaunas", country: "Lithuania", curr_latitude: 54.903931, curr_longitude: 23.959393, active: false },
	{ name: "Kazys Petraitis", city: "Vilnius", country: "Lithuania", curr_latitude: 54.672527, curr_longitude: 25.278509, active: false },
	{ name: "George Clooney", city: "Barselona", country: "Spain", curr_latitude: 54.857479, curr_longitude: 24.445061, active: false },
	{ name: "Titas Povilaitis", city: "Kaunas", country: "Lithuania", curr_latitude: 55.073103, curr_longitude: 24.279219, active: false },
])

locations = Location.create([
	{ latitude: 54.903931, longitude: 23.959393, city: "Kaunas", country: "Lithuania", guide_id: 1, time: "2019-05-07 19:32:42.470403" },
	{ latitude: 54.672527, longitude: 25.278509, city: "Vilnius", country: "Lithuania", guide_id: 2, time: "2019-01-07 19:32:42.470403" },
	{ latitude: 54.857479, longitude: 24.445061, city: "Kaisiadorys", country: "Lithuania", guide_id: 1, time: "2019-05-02 19:32:42.470403" },
	{ latitude: 55.073103, longitude: 24.279219, city: "Jonava", country: "Lithuania", guide_id: 2, time: "2019-05-06 19:32:42.470403" },
	{ latitude: 55.704197, longitude: 21.136484, city: "Klaipeda", country: "Lithuania", guide_id: 3, time: "2019-05-20 19:32:42.470403" },
	{ latitude: 54.555049, longitude: 23.353885, city: "Marijampole", country: "Lithuania", guide_id: 1, time: "2019-08-06 19:32:42.470403" },
	{ latitude: 55.243988, longitude: 24.771980, city: "Ukmerge", country: "Lithuania", guide_id: 3, time: "2019-05-12 19:32:42.470403" },
	{ latitude: 55.726266, longitude: 24.362778, city: "Panevezys", country: "Lithuania", guide_id: 4, time: "2019-05-13 19:32:42.470403" },
])
