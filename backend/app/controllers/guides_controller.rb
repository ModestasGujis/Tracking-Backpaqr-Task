class GuidesController < ApplicationController
  def index
    guides = GuideResource.all(params)
    respond_with(guides)
  end

  def show
    guide = GuideResource.find(params)
    respond_with(guide)
  end

  def create
    guide = GuideResource.build(params)

    if guide.save
      render jsonapi: guide, status: 201
    else
      render jsonapi_errors: guide
    end
  end

  def update
    guide = GuideResource.find(params)

    LineChannel.broadcast_to('line_channel', params["data"])

    if guide.update_attributes
      render jsonapi: guide
    else
      render jsonapi_errors: guide
    end
  end

  def destroy
    guide = GuideResource.find(params)

    if guide.destroy
      render jsonapi: { meta: {} }, status: 200
    else
      render jsonapi_errors: guide
    end
  end
end
