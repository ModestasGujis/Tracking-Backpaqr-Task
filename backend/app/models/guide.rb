class Guide < ApplicationRecord
    has_many :locations, dependent: :destroy
end
