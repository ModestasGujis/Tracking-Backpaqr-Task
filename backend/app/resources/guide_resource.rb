class GuideResource < ApplicationResource
  attribute :name, :string
  attribute :city, :string
  attribute :country, :string
  attribute :curr_latitude, :float
  attribute :curr_longitude, :float
  attribute :active, :boolean
  
  has_many :locations
end
