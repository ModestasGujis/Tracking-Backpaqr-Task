class LocationResource < ApplicationResource
  attribute :latitude, :float
  attribute :longitude, :float
  attribute :city, :string
  attribute :country, :string
  attribute :time, :datetime
  attribute :guide_id, :integer, only: [:filterable]
end
