FactoryBot.define do
  factory :guide do
    name { "MyString" }
    city { "MyString" }
    country { "MyString" }
  end
end
