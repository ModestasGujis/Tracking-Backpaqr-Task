FactoryBot.define do
  factory :location do
    latitude { 1.5 }
    longitude { 1.5 }
    city { "MyString" }
    country { "MyString" }
    time { "2019-06-13 23:24:30" }
    guide_id { 1 }
  end
end
