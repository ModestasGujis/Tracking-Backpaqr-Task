import React from 'react';
import '@babel/polyfill';

import { render } from 'react-dom';
import App from 'containers/App/App';

render(
	(
		<React.Fragment>
			<App />
		</React.Fragment>
	), document.getElementById('root'),
);
