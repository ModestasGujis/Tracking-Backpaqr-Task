import React from 'react';

export default React.createContext({
	guides: [],
	cableApp: {},
	updateGuides: () => {},
});
