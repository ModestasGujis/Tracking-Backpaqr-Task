import React, { useState } from 'react';

import {
	Grid,
} from '@material-ui/core';
import {
	ANY,
} from 'constants';

import MapContainer from 'components/MapContainer/MapContainer';
import GuideList from 'components/GuideList/GuideList';

import './Table.scss';
import LineWebSocket from '../LineWebSocket';

const Table = (props) => {
	const [toDisplay, setToDisplay] = useState(ANY);

	const displayGuidesLocations = (index) => {
		setToDisplay(index);
	};

	return (
		<div>
			<LineWebSocket />
			<Grid container className="grid">
				<Grid item xs={12}>
					<Grid
						container
						direction="row"
						justify="center"
						alignItems="stretch"
						className="containertrec"
					>
						<Grid id="map" xs={6} item>
							<MapContainer
								google={props.google}
								toDisplay={toDisplay}
							/>
						</Grid>
						<Grid id="guides" xs={6} item>
							<GuideList
								displayGuidesLocations={displayGuidesLocations}
								toDisplay={toDisplay}
							/>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</div>
	);
};

export default Table;
