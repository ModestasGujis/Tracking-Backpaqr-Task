import React from 'react';
import {
	AppBar,
	Toolbar,
} from '@material-ui/core';

import BackpaqrIcon from 'resources/BackpaqrIcon.svg';
import InputFields from 'components/InputFields/InputFields';

import './Header.scss';

const Header = (props) => {
	return (
		<AppBar position="static" color="inherit">
			<Toolbar className="appBar">
				<BackpaqrIcon className="backpaqrIcon" />
				<InputFields />
			</Toolbar>
		</AppBar >
	);
};

export default Header;
