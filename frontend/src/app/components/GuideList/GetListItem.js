import React from 'react';
import {
	Typography,
	ListItem,
	ListItemText,
} from '@material-ui/core';

export default function GetListItem(guide, open, handleClick) {
	return (
		<React.Fragment key={guide.id}>

			<ListItem
				button
				selected={open}
				onClick={() => handleClick(guide.id)}
			>
				<ListItemText inset primary={guide.name} secondary={guide.city} />
				<Typography className={guide.active ? 'activeStatus': 'inactiveStatus'} >
					{guide.active ? 'Active' : 'Inactive'}
				</Typography>
			</ListItem>

		</React.Fragment>
	);
}
