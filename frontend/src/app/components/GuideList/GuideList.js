import React, {
	useContext,
} from 'react';
import {
	Paper,
	List,
	ListSubheader,
	ListItem,
	ListItemText,
} from '@material-ui/core';
import {
	ANY,
} from 'constants';
import GetListItem from './GetListItem.js';

import './GuideList.scss';
import GlobalContext from 'contexts/GlobalContext';

const GuideList = (props) => {
	const context = useContext(GlobalContext);
	const {
		guides,
	} = context;

	const handleClick = (guideId) => {
		if (guideId !== props.toDisplay)
			props.displayGuidesLocations(guideId);
	};

	return (
		<div>
			<Paper id="list">
				<List
					component="nav"
					subheader={<ListSubheader component="div">Guide List</ListSubheader>}
				>
					<React.Fragment key={ANY}>
						<ListItem selected={ANY === props.toDisplay} button onClick={() => handleClick(ANY)}>
							<ListItemText inset primary="All guides" />
						</ListItem>
					</React.Fragment>
					{
						guides.map((guide) =>
							GetListItem(guide, guide.id === props.toDisplay, handleClick)
						)
					}
				</List>
			</Paper>
		</div>
	);
};

export default GuideList;
