import React from 'react';
import {
	InfoWindow,
} from 'google-maps-react';
import {
	Paper,
	Typography,
} from '@material-ui/core';

export default function GetInfoWindow(index, activeMarker, visible, name, city) {
	return (
		<InfoWindow
			key={index}
			marker={activeMarker}
			visible={visible}
		>
			<Paper>
				<Typography
					variant = 'headline'
					component = 'h6'
				>
					{name}
				</Typography>
				<Typography
					component='p'
				>
					{city}
				</Typography>
			</Paper>
		</InfoWindow>
	);
};
