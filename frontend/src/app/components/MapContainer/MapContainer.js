import React, { useState, useContext } from 'react';
import {
	Map,
	GoogleApiWrapper,
	Marker,
} from 'google-maps-react';
import {
	GOOGLE_MAPS_API_KEY,
	CURRENT_LATITUDE,
	CURRENT_LONGITUDE,
	DEFAULT_ZOOM,
	ANY,
} from 'constants';
import GetInfoWindow from './GetInfoWindow.js';

import './MapContainer.scss';
import GlobalContext from 'contexts/GlobalContext';

const mapStyles = {
	width: '40%',
	height: '70%',
	borderRadius: '25px',
	boxShadow: '0px 0px 50px -5px rgba(0,0,0,0.75)',
	marginLeft: '2em',
};

const MapContainer = (props) => {
	const context = useContext(GlobalContext);
	const {
		guides,
	} = context;
	const {
		toDisplay,
	} = props;

	const guideCount = guides.length;
	const [markers, setMarkers] = useState({
		showingInfoWindow: Array(guideCount).fill(false),
		activeMarker: Array(guideCount).fill({}),
	});

	const onMarkerClick = (index) => (props, marker, e) => {
		setMarkers((prevMarker) => {
			const {
				activeMarker,
				showingInfoWindow,
			} = prevMarker;
			activeMarker[index] = marker;
			showingInfoWindow[index] = true;
			return { activeMarker, showingInfoWindow };
		});
	};

	const onMapClick = (props) => {
		setMarkers((prevMarker) => {
			const { activeMarker } = prevMarker;
			let { showingInfoWindow } = prevMarker;
			showingInfoWindow = showingInfoWindow.fill(false);
			return { activeMarker, showingInfoWindow };
		});
	};

	const {
		activeMarker,
		showingInfoWindow,
	} = markers;

	return (
		<Map
			google={props.google}
			zoom={DEFAULT_ZOOM}
			style={mapStyles}
			onClick={onMapClick}
			initialCenter={{
				lat: CURRENT_LATITUDE,
				lng: CURRENT_LONGITUDE,
			}}
			className="map"
		>
			{
				guides.map((guide, index) => {
					return (toDisplay == guide.id || toDisplay == ANY) ? (
						<Marker
							key={index}
							onClick={onMarkerClick(index)}
							position={{ lat: guide.curr_latitude, lng: guide.curr_longitude }}
							name={guide.city}
						/>
					) : null;
				})
			}
			{
				guides.map((guide, index) => {
					return (toDisplay === guide.id || toDisplay == ANY) ? (
						GetInfoWindow(index, activeMarker[index], showingInfoWindow[index], guide.name, guide.city)
					) : null;
				})
			}
		</Map>
	);
};

export default GoogleApiWrapper({
	apiKey: GOOGLE_MAPS_API_KEY,
})(MapContainer);
