import React from 'react';

import GlobalContext from 'contexts/GlobalContext';

/* not working as functional component? */
class LineWebSocket extends React.Component {
	static contextType = GlobalContext;

	componentDidMount() {
		this.context.cableApp.cable.subscriptions.create({ channel: 'LineChannel' }, {
			received: (newGuide) => {
				this.context.updateGuides(newGuide);
			},
		});
	}

	render() {
		return (
			<div />
		);
	}
}
export default LineWebSocket;
