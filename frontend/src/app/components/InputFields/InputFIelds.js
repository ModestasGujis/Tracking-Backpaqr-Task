import React, { useState, useContext, useEffect } from 'react';
import {
	Typography,
	TextField,
	Button,
	MenuItem,
} from '@material-ui/core';

import { UpdateGuidesLocation } from 'apiUtils';
import GlobalContext from 'contexts/GlobalContext';

const InputFIelds = (props) => {
	const context = useContext(GlobalContext);

	const defaultGuide = {
		id: -1,
		name: 'Default',
		city: 'Default',
		country: 'Default',
		curr_latitude: 0,
		curr_longitude: 0,
	};

	const [currGuide, setCurrGuide] = useState(defaultGuide);

	const [latitude, setLatitude] = useState(currGuide.curr_latitude);
	const [longitude, setLongitude] = useState(currGuide.curr_longitude);
	const [city, setCity] = useState(currGuide.city);
	const [country, setCountry] = useState(currGuide.country);

	useEffect(() => { // initial guide
		if (currGuide.id === -1 && context.guides.length) {
			const newGuide = Object.assign({}, context.guides[0], { active: true });
			updateGuide(newGuide);
			setCurrGuide(newGuide);
		}
	}, [context.guides]);

	useEffect(() => {
		// activating new guide
		updateGuide(currGuide);

		setLatitude(currGuide.curr_latitude);
		setLongitude(currGuide.curr_longitude);
		setCity(currGuide.city);
		setCountry(currGuide.country);

		window.addEventListener('beforeunload', deactivateGuide); // handle page reload

		return () => {
			// deactivating old guide
			deactivateGuide(currGuide);
			window.removeEventListener('beforeunload', deactivateGuide);
		};
	}, [currGuide]);

	const deactivateGuide = () => {
		if (currGuide.id !== -1)
			updateGuide(Object.assign({}, currGuide, { active: false }));
	};

	const updateCurrGuide = (e) => {
		const targetGuide = context.guides.find((guide) => guide.name === e.target.value);
		if (currGuide.id === targetGuide.id) return; // pressed on self

		const newGuide = Object.assign({}, targetGuide, { active: true });
		setCurrGuide(newGuide);
	};

	// formats and updates guide
	const updateGuide = (newGuide) => {
		if (newGuide.id === -1) return; // invalid Guide

		const data = {
			type: 'guides',
			id: newGuide.id,
			attributes: Object.assign({}, newGuide),
		};

		delete data.attributes.id;
		UpdateGuidesLocation(newGuide.id, { data: data });
	};

	return (
		<React.Fragment>
			<span className="inputField">
				<Typography>Who am I:</Typography>
				<TextField
					select
					onChange={updateCurrGuide}
					value={currGuide.name}
				>
					{context.guides.map((guide) => (
						<MenuItem key={guide.id} value={guide.name}>
							{guide.name}
						</MenuItem>
					))}
				</TextField>
			</span>
			<span className="inputField">
				<Typography>Latitude:</Typography>
				<TextField
					onChange={(e) => setLatitude(e.target.value)}
					value={latitude}
				/>
			</span>
			<span className="inputField">
				<Typography>Longitude:</Typography>
				<TextField
					onChange={(e) => setLongitude(e.target.value)}
					value={longitude}
				/>
			</span>
			<span className="inputField">
				<Typography>City:</Typography>
				<TextField
					onChange={(e) => setCity(e.target.value)}
					value={city}
				/>
			</span>
			<span className="inputField">
				<Typography>Country:</Typography>
				<TextField
					onChange={(e) => setCountry(e.target.value)}
					value={country}
				/>
			</span>
			<Button
				onClick={() => updateGuide(Object.assign({}, currGuide,
					{
						curr_latitude: latitude,
						curr_longitude: longitude,
						city: city,
						country: country,
					}))}
				variant="outlined"
			>
				Update
			</Button>
		</React.Fragment>
	);
};

export default InputFIelds;
