import { API_BASE_URL } from 'constants/';


const request = (options) => {
	const headers = new Headers({
		'Content-Type': 'application/json',
	});
	const defaults = { headers };
	const fetchOptions = Object.assign({}, defaults, options);

	return fetch(fetchOptions.url, fetchOptions)
		.then((response) => {
			return response.json();
		});
};

export async function getGuides() {
	return await request({
		url: `${API_BASE_URL}/guides?sort=name`,
		method: 'GET',
	});
}

export async function UpdateGuidesLocation(id, body) {
	return await request({
		url: `${API_BASE_URL}/guides/${id}`,
		method: 'PUT',
		body: JSON.stringify(body),
	});
}

export function getGuidesWithLocations() {
	return request({
		url: `${API_BASE_URL}/guides_with_locations`,
		method: 'GET',
	});
}

export function getGuideWithLocations() {
	return request({
		url: `${API_BASE_URL}/guide_with_locations`,
		method: 'GET',
	});
}

export function getRecentLocations(count) {
	return request({
		url: `${API_BASE_URL}/rec_locations/${count}`,
		method: 'GET',
	});
}
