import React, { useState, useEffect } from 'react';

import Table from 'components/Table/Table';
import Header from 'components/Header/Header';
import { getGuides } from 'apiUtils/';

import Background from 'resources/Background.svg';
import GlobalContext from 'contexts/GlobalContext';

import actionCable from 'actioncable';

import './App.scss';

const App = (props) => {
	const [guides, setGuides] = useState([]);

	const cableApp = {};

	cableApp.cable = actionCable.createConsumer(`ws://${window.location.hostname}:9000/cable`);

	useEffect(() => {
		getGuidesFromServer();
	}, []);


	const getGuidesFromServer = () => {
		getGuides().then((response) => {
			/* console.log('mappinti duomenys is serverio', response.data.map((guide) => {
				guide.attributes.id = parseInt(guide.id);
				return guide.attributes;
			})); */
			setGuides(response.data.map((guide) => {
				guide.attributes.id = parseInt(guide.id);
				return guide.attributes;
			}));
		}).catch();
	};

	const updateAppStateLine = (data) => {
		const index = guides.findIndex((guide) => guide.id === data.id);

		const newGuides = [...guides];
		const newGuide = Object.assign({}, {id: data.id}, data.attributes);

		newGuides[index] = newGuide;

		setGuides(newGuides);
	};

	return (
		<div>
			<GlobalContext.Provider
				value={{
					guides: guides,
					cableApp: cableApp,
					updateGuides: updateAppStateLine,
				}}
			>
				<Background
					preserveAspectRatio="xMidYMid meet"
					className="background"
				/>
				<Header />
				<Table
					google={props.google}
				/>
			</GlobalContext.Provider>
		</div >
	);
};

export default App;
