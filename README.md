Before starting  
	• configure backend/config/database.yml accordingly  
	• cd backend  
	• rails db:create  
	• rails db:migrate  
	• rails db:seed  
	• cd ../frontend  
	• npm install  

To start rails server run **rails server -p 9000** in backend directory  
To start npm run **npm start** in frontend directory  
View website at http://localhost:3000/  
